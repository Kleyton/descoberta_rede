#!/usr/bin/python
#
#  Kleyton Leite
#  arquivo:args.json
#  Abre arquivo json e extrai argumentos
#


import json


class arg_json(object):
    def __init__(self, file):
        self.data = {}
        try:
            arq = open(self, file, "r")
        except IOError:
            print "Erro ao abrir arquivo"
            return False
        self.data = json.load(arq)
        return True

    """
        Retorna uma lista de argumentos encontrados no arquivo
    """
    def arg_found(self):
        return self.data.keys()

    def arg(self, key):
        return self.data[key]