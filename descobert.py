#!/usr/bin/python
#
#   Kleyton Leite
#   
#   Arquivo: descobert.py
#
#   Descobre dispositivos na rede

import json

try:
    import nmap
except ImportError:
    print("Install python-nmap")
    
class Descobert:
    
    def __init__(self, rede, mascara):
        self.Scan = nmap.PortScanner()
        self.Scan.scan(rede+'/'+mascara)
        
    """
        Mostra hosts descobertos
    """
    def hosts(self):
        return self.Scan.all_hosts()
    