#!/usr/bin/python
#
#   Kleyton Leite
#   Arquivo: main.py
#

import getpass
import os
import argparse
import sys
import io
from descobert import Descobert
from conection_ssh import Connect
from conf_txt import _Colors
from args_json import arg_json

scor = _Colors()

def get_lines(file):
    try:
        arq = io.open(file, "r")
    except IOError:
        print(scor.RED() + "Erro ao ler arquivo")
        sys.exit()
    command=['\n']
    for line in arq:
        command.append(line.encode('utf8'))
    return command[1:]


def devices_off(dev_off):
    #  Dispositivos nao acessiveis
    try:
        a_log = io.open(log, "r+", encoding="utf-8")
    except IOError:
        print("Erro ao criar arquivo de log")
    else:
        a_log = io.open(log, "a+", encoding="utf-8")
    a_log.write("Dispositivos offline".decode('utf-8'))
    print(scor.RED()+'Dispositivos inacessiveis')
    for i in dev_off:
        print(i)
        a_log.write(str(i).decode('utf-8'))
    a_log.close()
    
def write_file(lista, file):
    try:
        arq = io.open(file, "a+", encoding="utf-8")
    except IOError:
        print("Erro ao criar arquivo de host")
    else:
        arq = io.open(file, "w+", encoding="utf-8")
    print(scor.BLUE()+"Escrevendo em arquivo de hosts")
    for i in lista:
        arq.write(str(i+'\n').decode('utf-8'))
    arq.close()

#  Opcoes de linha de comando
options = argparse.ArgumentParser(
    prog='Obtencao de configuracao e gerenciamento remoto',prefix_chars='-')
options.add_argument('-d', '--dir_conf', type=str,
                     help='Definir local de armazenagem das configuracoes',
                     default='.')
options.add_argument('-n', '--network', type=str,
                     help='Endereco da rede dos dispositivos')
options.add_argument('-m', '--mask', type=str,
            help='Define mascara de procura para a rede definida em -n')
options.add_argument('-o', '--output_log', default='log.txt',
                     help='Arquivo de log para reportar erros e warnings')
options.add_argument('-c', '--command', default=None, type=str,
    help='Define comandos em um arquivo a serem executados nos dispositivos listados')
options.add_argument('-s', '--save_config', action='store_true',
                     help='Salvar configuracoes no local informado em -d')
options.add_argument('-u', '--username', required=True, type=str,
                     help='Usuario para os hosts se conectarem')
options.add_argument('-p', '--password', action='store_true',
                     help='Senha de acesso aos dispositivos')
options.add_argument('-f', '--file', type=str, help='Caminho para arquivo com hosts')
options.add_argument('-g', '--gravar_hosts', type=str, 
                     help='gravar hosts no arquivo especificado')
options.add_argument('-j', "--arg_json", type=str, help="Arquivo JSON com comandos")

if options.parse_args().arg_json is not None:
    #  carregar arquivos da classe JSON
    #   verifica quais comando foram passados pelo JSON
    arg_file = arg_json(options.parse_args().arg_json)
    for i in arg_file.arg_found():
        i.encode("utf-8")
        if i == "output_log":
            file_log = arg_file.arg(i.decode("utf-8"))
        if i == "command":
            l_cmd = arg_file.arg(i.decode("utf-8"))
        if i == "save_config":
            save_config = True
        if i == "username":
            username = arg_file.arg(i.decode("utf-8"))
        if i == "password":
            password = True
        if i == "file":
            l_hosts = arg_file.arg(i.decode("utf-8"))
        if i == "gravar_hosts":
            arq_host = arg_file.arg(i.decode("utf-8"))
        else:
            print("Comando nao reconhecido")
else:
    l_cmd = options.parse_args().command
    l_hosts = options.parse_args().file
    local = options.parse_args().dir_conf
    log = options.parse_args().output_log
    net = options.parse_args().network
    mask = options.parse_args().mask
    save_config = options.parse_args().save_config
    username = options.parse_args().username
    arq_host = options.parse_args().gravar_hosts
    pass_request = options.parse_args().password

if pass_request:
    password = getpass.getpass("Entre com a senha de acesso aos dispositivos:")


if not os.path.exists(local):
    print("Diretorio Inexistente")
    sys.exit()

dev_on = [' ']
dev_off = [' ']

if l_cmd is not None:    
    cmd = get_lines(l_cmd)
if l_hosts is not None:
    devices = get_lines(l_hosts)
    for i in devices:
        dev_on.append(i)
else:
    devices = Descobert(net, mask)
    dev_on = devices.hosts()

if arq_host is not None:
    write_file(dev_on, arq_host)

print (scor.BOLD()+'Dispositivos online: ')
for i in dev_on:
    print(scor.BLUE()+'-> %s'%i)

for i in range(0, len(dev_on)):
    conexao = Connect(dev_on[i], username, password)
    if conexao.preparar_shell():
        print(scor.RESET()+scor.YELLOW()+ 
              'Obtendo nome do dispositivo %d' % i)
        nome = conexao.get_name(dev_on[i])
        if nome is not None:
            #  Salvar configuracoes
            if save_config:
                conexao.save_conf(filename=(local+'/'+nome))
                print(scor.GREEN()+'* Configuracao salva: %s\n'%nome)
            # Envia comandos ao dispositivo
            if l_cmd is not None:
                #  Preparacao do arquivo de log
                try:
                    a_log = io.open(log, "w+", encoding="utf-8")
                except IOError:
                    print("Erro ao criar arquivo de log")
                else:
                    a_log = io.open(log, "a", encoding="utf-8")
                conexao.exec_command(cmd, a_log)
                print (scor.GREEN()+'* Comandos aplicados a %s'%nome)
        else:
            continue
    else:
        #  fazer lista de dispositivos inacessiveis
        #  por ssh
        dev_off.append(dev_on[i])
        pass

devices_off(dev_off)

