#!/usr/bin/python
#
#   Kleyton Leite
#   Arquivo: conf_txt.py
#

class _Colors(object):
    def RED(self):
        return "\033[1;31m"  
    def BLUE(self):
        return "\033[1;34m"
    def CYAN(self):
        return "\033[1;36m"
    def GREEN(self):
        return "\033[0;32m"
    def RESET(self):
        return "\033[0;0m"
    def BOLD(self):
        return "\033[;1m"
    def REVERSE(self):
        return "\033[;7m"
    def YELLOW(self):
        return "\033[0;93m"