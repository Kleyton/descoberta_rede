#!/usr/bin/python
#
#   Kleyton Leite
#
#    Arquivo: file_remote.py
#
#   Conexao com os dispositivos afim de obter as configuracoes
#

import ftplib


class file_remote(object):
    def __init__(self, host, user, passwd):
        self.host = host
        self.user = user
        self.passwd = passwd

    def connection(self):
        try:
            self.ftp = ftplib.FTP(self.host, self.user, self.passwd)
        except ftplib.error_reply:
            print("Resposta inesperada!!!")
            return False
        except ftplib.error_temp:
            print("Tempo excedido!!!")
            return False
        except ftplib.error_perm:
            print("Erro permanente!!!")
            return False
        except ftplib.error_proto:
            print("Erro em protocolo")
            return False

    def get_file(self, name_remote, name_local):
        flocal = open('./'+name_local, 'wb')
        self.ftp.retrbinary(name_remote, flocal.write)
        return None

    def files_remotes(self):
        return self.ftp.dir()

    def __del__(self):
        self.ftp.quit()
        self.ftp.close()
