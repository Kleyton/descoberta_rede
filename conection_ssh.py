#!/usr/bin/python
#
#   Kleyton Leite
#   
#   Arquivo: conection_ssh.py
#
#   Conecta aos equipamentos via SSH

from datetime import datetime
import sys

try:
    import paramiko
except ImportError:
    print("Install python-paramiko")
    sys.exit()
try:
    import os
    import io
    import time
    import socket
except ImportError:
    print("Erro padrao de importacao")

try:
    import re
except ImportError:
    print("Erro ao importar modulo regex")
    sys.exit()


class Connect(object):
    def __init__(self, host, user, password):
        self.host = host
        self.user = user
        self.password = password
        self.data = datetime.now()

    """
        Obtem status do dispositivo quanto ao ssh 
    """
    def preparar_shell(self):
        print("Conectando a %s" % str(self.host))
        try:
            self.client = paramiko.client.SSHClient()
        except paramiko.SSHException:
            print("Dispositivo nao acessivel por ssh")
            return False
        except paramiko.ssh_exception.NoValidConnectionsError:
            print("Erro ao validar conexao")
        self.client.set_missing_host_key_policy(
                    paramiko.client.AutoAddPolicy())
        try:
            self.client.connect(self.host, username=self.user,
                                password=self.password, look_for_keys=False,
                                auth_timeout=2)
        except paramiko.SSHException:
            print("Dispositivo nao acessivel por ssh")
            return False
        except paramiko.ssh_exception.NoValidConnectionsError:
            print("Erro ao validar conexao")
            return False
        except socket.error:
            print("Erro de conexao")
            return False
        except EOFError:
            print("Erro EOF")
            return False

        self.channel = self.client.get_transport().open_session()
        self.channel.get_pty()
        self.channel.invoke_shell()
        print('Conexao estabelecida com o %s'%self.host)
        return True

    """
        Recebimento via SSH
    """             
    def receive_ssh(self):
        output = self.channel.recv(1048576)
        saida = str(output)
        return saida

    """
        Obter respostas do host
    """
    def respost(self, cmd, log):
        saida = ''
        #  exception do paramiko
        try :
            self.channel.send(cmd.decode('utf-8')+'\n')
        except socket.error:
            print("Conexao fechada")
            self.client.close()
            sys.exit(-1)
        
        
        while not self.channel.recv_ready():
            time.sleep(1)

        while True:
            time.sleep(2.5)
            try:
                saida_n = self.receive_ssh()
            except paramiko.SSHClient:
                print("Falha em enviar comandos ao dispositivo")
                return None
            if str(saida_n) == str(saida):
                return saida, True
            else:
                saida = saida_n
                words = saida.split()
                if 'unrecognized' in words:
                    print("Falha ao aplicar comando")
                    return self.respost('display version', log), False
                else:
                    return saida, True
        return None
    """
        Salvar configuracao de switchs
            - Comando padrao para switchs 4200G e 5130
    """
    def save_conf(self, command=[], filename=None, l=None):

        #  Verficacao do log
        if l is None:
                close = True
                l = 'log.txt'

        if not os.path.exists('./'+l):
            # cria arquivo para guardar log
            try:
                log = io.open(l, "w+", encoding="utf-8")
            except IOError:
                print("Erro ao criar arquivo de log")
        else:
            pass
            # arquivo ja existe, adiciona no final
            try:
                log = io.open(l, "a", encoding="utf-8")
            except IOError:
                print("Erro ao adicionar informacoes ao log")
        log.write("%s:%s - %s/%s/%s : ####### \n Processando: %s \n #######" % 
                  (self.data.minute, self.data.hour, self.data.day,
                   self.data.month, self.data.year, filename.decode('utf-8')))

        if filename is None:
            n_arq = 'conf.txt'
        else:
            n_arq = filename+'.conf'

        if not os.path.exists('./'+n_arq):
            # cria arquivo para guardar conf
            try:
                arq = io.open(n_arq, "w+", encoding="utf-8")
            except IOError:
                print("Erro ao criar arquivo")
        else:
            try:
                arq = io.open(n_arq, "a+", encoding="utf-8")
            except IOError:
                print("Erro ao adicionar informacoes ao arquivo existente")

        if len(command) is 0:
            cmd = ['undo terminal monitor', 'screen-length disable',
                   'dis cu']
        else:
            cmd = command

        self.exec_command(cmd, arq, log)
        log.write("%s:%s - %s/%s/%s : ####### \n Fim do processamento: %s \n #######" %
                  (self.data.hour, self.data.minute, self.data.day,
                   self.data.month, self.data.year,filename.decode('utf-8')))
        if close:
            log.close()

    """
        Executa cadeia de comandos e salva resposta em um arquivo especificado
    """
    def exec_command(self, cmd, filename=None, log=None):
        i = 0
        #  ???? e se nao for informado o arquivo?
        while i < len(cmd):
            texto, resposta = self.respost(cmd[i], log)
            if not resposta:
                if log is not None:
                    # log.write("%s:%s - %s/%s/%s : Problemas ao obter resposta do dispositivo" % 
                    #             (self.data.minute, self.data.hour, self.data.day, self.data.month,
                    #              self.data.year))
                    log.write(str("Problemas ao obter resposta do dispositivo").decode('utf-8'))
                
            else:
                if log is not None:
                    log.write("%s:%s - %s/%s/%s : ####### \n Fim do processamento: %s \n #######" %
                  (self.data.hour, self.data.minute, self.data.day,
                   self.data.month, self.data.year,str(filename).decode('utf-8')))
                    #  log.write("Resposta: %s" % resposta.decode('utf-8'))
                if filename is not None:
                    filename.write(texto.decode('utf-8'))
                i=i+1

    """
        Obtem nome do dispositivo
    """
    def get_name(self, id, l=None):

        # Verificacao do log
        if l is None:
                close = True
                l = 'log.txt'
        if not os.path.exists('./'+l):
            # cria arquivo para guardar conf
            try:
                log = io.open(l, "w+", encoding="utf-8")
            except IOError:
                print("Erro ao criar arquivo de log")
        else:
            log = io.open(l, "a", encoding="utf-8")
        cmd = ['\n \n', '\n \n']
        for i in cmd:
            retorno, status = self.respost(i,log)
            if not status:
                log.write("Erro ao obter resposta do dispositivo: %d" % id)
                break
        if status:
            regex = r"[^\*\<]*[\d][^\>]"
            matches = re.finditer(regex, retorno, re.MULTILINE)

            name = id

            for matchNum, match in enumerate(matches, start=1):
                name = match = match.group()
            log.write(id + " -> " + name.decode('utf-8'))
            if close:
                log.close()

            return name
        else:
            return None

    def __del__(self):
        self.client.close()
